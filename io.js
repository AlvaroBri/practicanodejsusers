//Libreria para tratamiento de escritura en disco
const fs = require('fs');

function writeUserDatatoFile(data){
  console.log("WriteUserDatatoFile");

  //Parseo a Json del Array javascript
  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
    function(err){
      if(err){
        console.log(err);
      } else{
        console.log("Usuario persistido");
      }
    }
  )
}

module.exports.writeUserDatatoFile = writeUserDatatoFile;
