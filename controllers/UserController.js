const io = require('../io.js');
const crypt = require('../crypt');

/*Carga libreria request-json*/
const requestJson = require('request-json');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuabi10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getUsersV1 (req, res){
  console.log("GET /apitechu/v1/users");
  /*Devuelve el fichero, el root dirname posiciona la busqueda del fichero en la ruta de ejecucion*/
  //res.sendFile('usuarios.json', {root:__dirname});
  var users = require('../usuarios.json');
  var result = {};
  var count = req.query.$count;
  var top = req.query.$top;

  //Filtro Count
  if (count == "true"){
    var usersLength = users.length;
    result.count= usersLength;
  }

  //Filtro Top
  if (top){
    var userTop = users.slice(0,top)
    result.users = userTop;
  }else{
    result.users = users;
  }

 res.send(result);
}

function getUsersV2 (req, res){
  console.log("GET /apitechu/v2/users");

  //Crea el cliente http indicando la url base de las peticiones
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("user?" + mLabAPIKey,
    function(err, resMLab, body){
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }
      res.send(response);
    }
  )
}

function getUserByIdV2(req, res){
  console.log("GET /apitechu/v2/users/:id");

  var id = req.params.id;
  var query = 'q={"id":' + id + '}';
  console.log("La consulta es " + query);

  //Crea el cliente http indicando la url base de las peticiones
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if(err){
        var response = {
          "msg" : "Error obteniendo usuarios"
        }
        res.status(500);
      }else{
        if(body.length > 0){
          var response = body[0];
        }else{
          var response =  {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}

function createUserV1(req, res){
  console.log("POST /apitechu/v1/users");

  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email
  }

  //Cargamos el array de usuarios del fichero
  var users = require('../usuarios.json');
  users.push(newUser);
  io.writeUserDatatoFile(users);
  console.log("Usuario añadido con éxito");

  res.send({"msg" : "Usuario añadido con éxito"});
}

function createUserV2(req, res){
  console.log("POST /apitechu/v2/users");

  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  console.log(req.body.password);

  var newUser = {
    "id": req.body.id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": crypt.hash(req.body.password)
  }

  //Crea el cliente http indicando la url base de las peticiones
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.post("user?" + mLabAPIKey, newUser,
    function(err, resMLab, body){
      console.log("Usuario creado en Mlab");
      res.status(201).send({"msg" : "Usuario guardado"});
    }
  )
}

function deleteUserV1(req,res){
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("id es " + req.params.id);

  var users =require('../usuarios.json');
  var deleted = false;

  // 1 For normal
  /*
  for (let i = 0; i < users.length; i++){
    if (users[i].id == req.params.id){
      users.splice(i,1);
      deleted = true;
      break;
    }
  }
  */

  // 2 for each
  /*
  users.forEach(function(user, index) {
    if(user.id == req.params.id){
      users.splice(index,1);
      deleted = true;
    }
  })
  */

  // 3 for in
  /*
  for (let index in users){
    if(users[index].id == req.params.id){
      users.splice(index,1);
      deleted = true;
      break;
    }
  }
  */

  //4 for of
  /*
  for(let [index,user] of users.entries()){
    if(user.id == req.params.id){
      users.splice(index,1);
      deleted = true;
      break;
    }
  }
  */

  //5 findIndex
  var index = users.findIndex(function findId(element){ return element.id == req.params.id;});

  if (index > 0){
    users.splice(index,1);
    deleted = true;
  }

  //Elimina el n elemento
  //var results = nicknames.filter(function (nickname) { return nickname.id == '200'; });
  //var firstObj = (results.length > 0) ? results[0] : null;
  //users.splice(req.params.id - 1,1);

  if (deleted) {
      io.writeUserDatatoFile(users);
  }

  var msg = deleted ? "Usuario borrado" : "Usuario no encontrado"
  res.send({"msg" : msg});
}

module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV1 = createUserV1;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1 = deleteUserV1;
