const io = require('../io.js');
const crypt = require('../crypt');

/*Carga libreria request-json*/
const requestJson = require('request-json');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuabi10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

//Login
function loginV1(req,res){
  console.log("POST /apitechu/v1/login");

  console.log("email: " + req.body.email);
  console.log("password: " + req.body.password);

  var users =require('../usuarios.json');
  var login = false;

  for (var i = 0; i < users.length; i++){
    if (users[i].email == req.body.email && users[i].password == req.body.password){
      console.log("Usuario encontrado: " + users[i].id);
      users[i].logged = true;
      login = true;
      break;
    }
  }

  if (login) {
    io.writeUserDatatoFile(users);
    res.send({"mensaje" : "Login correcto", "idUsuario" : users[i].id});
  }else{
    res.send({"mensaje" : "Login incorrecto"});
  }
}

//Login
function loginV2(req,res){
  console.log("POST /apitechu/v2/login");

  console.log("email: " + req.body.email);
  console.log("password: " + req.body.password);

  var query = 'q={"email": "' + req.body.email + '"}';

  //Crea el cliente http indicando la url base de las peticiones
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if(body.length > 0){
        console.log("Email encontrado");
        console.log(body[0].password);
        if (crypt.checkPassword(req.body.password, body[0].password)){
          console.log("Password coincide");
          var id = body[0].id;
          query = 'q={"id":' + id + '}';
          var putBody = '{"$set":{"logged":true}}';
          httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(err2, resMLab2, body2){
              if (!err2){
                console.log("Client logado");
                res.send({"mensaje" : "Login correcto", "idUsuario" : id});
              }
            }
          )
        }else{
          console.log("Password no coincide");
          res.status(401);
          res.send({"mensaje" : "Login incorrecto"});
        }
      }else{
        console.log("Email no encontrado");
        res.status(401);
        res.send({"mensaje" : "Login incorrecto"});
      }
    }
  )
}

//Logout
function logoutV1(req, res){
  console.log("POST /apitechu/v1/logout");
  console.log("id es " + req.params.id);

  var users = require('../usuarios.json');
  var logout = false;

  for (var i = 0; i < users.length; i++){
    if (users[i].id == req.params.id && users[i].logged == true){
      console.log("Usuario encontrado: " + users[i].id);
      delete users[i].logged;
      logout = true;
      break;
    }
  }

  if (logout) {
    io.writeUserDatatoFile(users);
    res.send({"mensaje" : "Logout correcto", "idUsuario" : users[i].id});
  }else{
    res.send({"mensaje" : "Logout incorrecto"});
  }
}

//Logout
function logoutV2(req, res){
  console.log("POST /apitechu/v2/logout");
  console.log("id es " + req.params.id);

  var query = 'q={"id": ' + req.params.id + '}';
  var putBody = '{"$unset":{"logged":""}}';

  //Crea el cliente http indicando la url base de las peticiones
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if(body.length > 0){
        console.log("Id encontrado");
        console.log(body[0].logged);
        if (body[0].logged){
          console.log("Esta logado");
          var id = body[0].id;
          httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(err2, resMLab2, body2){
              if (!err2){
                console.log("Client deslogado");
                res.send({"mensaje" : "Logout correcto", "idUsuario" : id});
              }
            }
          )
        }else{
          console.log("No esta logado");
          res.status(403);
          res.send({"mensaje" : "Logout incorrecto"});
        }
      }else{
        console.log("Id no encontrado");
        res.status(403);
        res.send({"mensaje" : "Logout incorrecto"});
      }
    }
  )
}

module.exports.loginV1 = loginV1;
module.exports.loginV2 = loginV2;
module.exports.logoutV1 = logoutV1;
module.exports.logoutV2 = logoutV2;
