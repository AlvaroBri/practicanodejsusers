const io = require('../io.js');
const crypt = require('../crypt');

/*Carga libreria request-json*/
const requestJson = require('request-json');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuabi10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getByUserIdV1(req, res){
  console.log("GET /apitechu/v1/accounts/:id");

  var id = req.params.id;
  var query = 'q={"idUsuario":' + id + '}';
  console.log("La consulta es " + query);

  //Crea el cliente http indicando la url base de las peticiones
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("account?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body){
      if(err){
        var response = {
          "msg" : "Error obteniendo cuentas"
        }
        res.status(500);
      }else{
        if(body.length > 0){
          var response = body;
        }else{
          var response =  {
            "msg" : "Cuentas no encontradas"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}

module.exports.getByUserIdV1 = getByUserIdV1;
