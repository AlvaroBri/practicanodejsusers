//Carga configuración por defecto gestión de variables de entorno
require('dotenv').config();

/*Carga libreria express*/
const express = require('express');
/*Inicia el framework express*/
const app = express();

/*Inicializa el puerto si tiene valor y sino lo tiene, carga por defecto el puerto 3000*/
const port = process.env.PORT || 3000;
//Carga controlador de usuarios para poder usar sus funciones
const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');


var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type");

 next();
}


//Procesado del body cuando es json
app.use(express.json());
//Habilida el crossdomain
app.use(enableCORS);
/*Inicializa el servidor escuchando en el puerto*/
app.listen(port);

console.log("API escuchando en el puerto " + port);

/*Instancia metodo get, con la ruta indica y el metodo que lanza*/
app.get('/apitechu/v1/hello',
  function(req, res){
    console.log("GET /apitechu/v1/hello");
    res.send({"msg" : "Hola desde API TechU"});
  }
)

app.get('/apitechu/v1/users', userController.getUsersV1);

app.get('/apitechu/v2/users', userController.getUsersV2);

app.get('/apitechu/v2/users/:id', userController.getUserByIdV2);

app.post('/apitechu/v1/users', userController.createUserV1);

app.post('/apitechu/v2/users', userController.createUserV2);

app.delete("/apitechu/v1/users/:id", userController.deleteUserV1);



app.post('/apitechu/v1/login', authController.loginV1);

app.post('/apitechu/v2/login', authController.loginV2);

app.post('/apitechu/v1/logout/:id', authController.logoutV1);

app.post('/apitechu/v2/logout/:id', authController.logoutV2);


app.get('/apitechu/v1/accounts/:id', accountController.getByUserIdV1);


app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res){
    console.log("Parametros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);

    res.send({"msg" : "monstruo"});
  }
)
